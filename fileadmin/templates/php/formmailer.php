<?php

// ======= Konfiguration:

$mailTo = "info@drechslerei-fritz.com";
$mailToCustumer = "";
$mailFromCustumer = '"Drechslerei Fritz" <info@drechslerei-fritz.com>';
$mailFrom = "";
$mailSubject = 'Bestellung';
$mailSubjectCustumer = 'Meine Bestellung';
$returnPage = 'http://www.drechslerei-fritz.com/de/produkte/bestellen/danke.html';
$returnErrorPage = 'http://server/Fehler-aufgetreten.html';
$mailText = "";

// ======= Text der Mail aus den Formularfeldern erstellen:

// Wenn Daten mit method="post" versendet wurden:
if (isset($_POST)) {
    // alle Formularfelder der Reihe nach durchgehen:
    $mailToCustumer = $_POST['Email'];
    $mailFrom = $_POST['Vorname'] . " " . $_POST['Nachname'] . " <" . $_POST['Email'] . ">";
    foreach ($_POST as $name => $value) {
        // Wenn der Feldwert aus mehreren Werten besteht:
        // (z.B. <select multiple>)
        if (is_array($value)) {
            // "Feldname:" und Zeilenumbruch dem Mailtext hinzufügen
            $mailText .= $name . ":\n";
            // alle Werte des Feldes abarbeiten
            foreach ($valueArray as $entry) {
                // Einrückungsleerzeichen, Wert und Zeilenumbruch
                // dem Mailtext hinzufügen
                $mailText .= "   " . $value . "\n";
            } // ENDE: foreach
        } // ENDE: if
        // Wenn der Feldwert ein einzelner Feldwert ist:
        else {
            // "Feldname:", Wert und Zeilenumbruch dem Mailtext hinzufügen
            $mailText .= $name . ": " . $value . "\n";
        } // ENDE: else
    } // ENDE: foreach
} // if

//FOOTER
$mailTextCustumer = $mailText . "\n\nADRESSE:\n39058 Sarntal\nSteet 37a\nItalien";

// BANKDATEN
$mailTextCustumer .= "\n\nBANKDATEN:\nIBAN: IT58G0823358871000301000110\nBIC: RZSBIT21132";
// ======= Korrekturen vor dem Mailversand

// Wenn PHP "Magic Quotes" vor Apostrophzeichen einfügt:
if (get_magic_quotes_gpc()) {
    // eventuell eingefügte Backslashes entfernen
    $mailtext = stripslashes($mailtext);
}

// ======= Mailversand

// Mail versenden und Versanderfolg merken
$mailSent = @mail($mailTo, $mailSubject, $mailText, "From: " . $mailFromCustumer);
$mailSentCustumer = @mail($mailToCustumer, $mailSubjectCustumer, "IHRE EINGEGEBENEN INFORMATIONEN:\n" . $mailTextCustumer, "From: " . $mailFromCustumer);

// ======= Return-Seite an den Browser senden

// // Wenn der Mailversand erfolgreich war:
if ($mailSent == true) {
    // Seite "Formular verarbeitet" senden:
    header("Location: " . $returnPage);
}
// Wenn die Mail nicht versendet werden konnte:
// else {
//    // Seite "Fehler aufgetreten" senden:
//    header("Location: " . $returnErrorPage);
// }

// ======= Ende

exit();
